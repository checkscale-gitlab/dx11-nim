# DX11

Docker X11 (dx11) is a command-line desktop session manager for Linux.

Bringing the goodies of [Docker](htps://www.docker.com) to the X11 world, DX11 is built on top of [x11docker](https://github.com/mviereck/x11docker) to let you run full sessions in a similar way as you would run a backend service.

DX11 is on alpha state and it heavily depends on the features offered by `x11ocker`. This tool is a native convenience wrapper to allow for fast management and access to desktop sessions.

DX11 can be a fancy replacement for security-aware users that rely on systems like Qubes OS or traditional virtual machines.

## Key features

* Scriptable, reproduceable and predictable desktop environments on Linux
    * Fresh desktops are one command away
* Share development setups with the whole team
    * Everyone can work with the exact same tools
    * No setup overhead for newcomers
    * Flexibility to tweak custom settings on top
* Native-like, snappy performance
    * Unlike VirtualBox, VMWare, etc. sessions use the host's Linux Kernel
    * Memory efficient: Docker uses the main system memory on demand, whereas VM's allocate a fixed amount on startup
* Security
    * Immutable base OS's (pretty much like Fedora Silverblue)
    * Home folder encryption (optional)
    * Tor networking (optional)
    * Isolated sandboxed sessions
* Persistent home folder
* Create and run multiple sessions at the same time

## Install

[Download DX11](https://gitlab.com/stack-me-up/dx11-nim/-/jobs/artifacts/master/download?job=dx11-build), extract it and copy `dx11` on a folder from your `$PATH` like `/usr/local/bin`

## Usage

### New session

To create a new session:

```
$ dx11 new <session-name> <path-to-Dockerfile> [--tor] [--encrypt]

# example
$ dx11 new mysession ./examples/ubuntu-mate/Dockerfile
```

This will create a session named `mysession` (or update it if it exists), build the related Docker image and store the config flags that you pass to `dx11`.

By default:
* The home folder encryption is disabled
* Tor networking is disabled

When a session already exists:
* Home data is preserved
* The encryption status can't be changed
* The the Tor settings are updated

### Running a session

```
$ dx11 run <session-name>

# example
$ dx11 run mysession
```

Your DX11 session should appear on a new virtual terminal.

Note: Make sure to **Log out** when you are done working with it

### Attaching as root

You can attach to an already active session and run system-wide commands as `root`.

```
$ dx11 exec <session-name> <command> [cmd-params]

# example
$ dx11 exec mysession apt install make
```

Note: Unless you use `docker commit`, your changes to the system will expire as soon as your regular user exits the dx11 session.

### List available sessions

```
$ dx11 list
```

### Rebuild an existing session

Docker images are rebuilt based on the `Dockerfile` contents and on the files being manually added to the system. However, if external content changes (`apt update`), Docker has no way to know about such changes if the base image is unchanged.

If for any reasons you need to force a full rebuild, you can simple run:

```
$ dx11 rebuild <session-name>

```

You will be offered the option to tag the current Docker image as a backup, so you can still use it later on.

### Change settings

To change a session's settings, you can run:

```
$ dx11 set <session-name> --tor
Using Tor: true
Done

$ dx11 set <session-name>
Using Tor: false
Done
```

New settings may be added in the future.

### Delete a session

```
$ dx11 delete <session-name>
Do you want to remove the session's Docker image? (y/N) 

This will run
$ sudo rm -Rf /home/user/.local/share/dx11/<session-name>

Do you really want to remove the session's data files?
This action cannot be undone (y/N) 
```

### Cleanup unused Docker images

```
$ dx11 prune
WARNING! This will remove all dangling images.
Are you sure you want to continue? [y/N]
```

This is simply an alias for `sudo docker image prune`

## Requirements

* [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [X11Docker](https://github.com/mviereck/x11docker#shortest-way-for-first-installation)
* [Ecryptfs-simple](https://software.opensuse.org/download.html?project=home%3Aobs_mhogomchungu&package=ecryptfs-simple) (if you use encryption)

DX11 can install `x11docker` and `ecryptfs-simple` for you if they are missing on your system.

## Folders

For every session, `dx11` creates a folder on `~/.local/share/dx11/<session-name>`

```
$ ls ~/.local/share/dx11/test/
config.json  docker/  home-mount/  home-src/  share/
```

* `docker` contains a copy of the `Dockerfile` used to build the current image and files on the same folder that might have been used on the Docker build process. This folder allows to rebuild the session in the future.
* `home-src` is the path where the guest home folder is kept
    * If encryption is enabled, the encrypted data is stored here
* `home-mount` is used when encryption is enabled
    * Decrypted files are mounted on this folder and exposed to the guest session

### Sharing files
* `~/Public/DX11` allows to exchange files between the host and guest sessions
    * The path is the same on both sides

## Cool use cases

* Run *dangerous* commands without the fear of breaking your system. Revert to any prior Docker image and start where you left off.
* Try bloated/unsafe software on a clone of your workstation and drop the changes when you are done evaluating.
* Copy a session's data (`~/.local/share/dx11/mysession/*`) to a brand new computer, rebuild the session image, run it and continue working as if nothing happened.
* Post the Dockerfile of your dev session on a team repo. Let your teammates clone it, run it, fork it, pull new versions and upgrade from it, all while preserving their local data.
* Keep 4 minimal isolated environments for your (1) sysadmin job, (2) devops freelance project, (3) development box for your side projects and (4) personal desktop for surfing the web.
* Forget about SSH key management by having every session with its corresponding keyfiles.

## Provided examples

See `examples/` and customize a `Dockerfile` to your needs:

* Ubuntu Mate
    * Fast minimal desktop with essential tools for standard usage
* Ubuntu Gnome
* Deepin
* Ubuntu Mate Dev
    * Standard Mate desktop, plus VS Code, Go, NodeJS, Android SDK, Flutter, Nim, net tools

You can also build your `Dockerfile` on top of [x11docker's images](https://hub.docker.com/u/x11docker/#!) on Docker Hub.

Contribute your own!

## Known limitations

* The regular `user` account has no password on `x11docker`
    * If you lock the guest screen, you will not be able to unlock it
* The regular `user` account does not have `sudo` access
    * Use `dx11 exec` instead
* USB Android development is not available, for security reasons
    * Use `adb tcpip` and `adb connect` to work with physical devices via wifi
    * Tor networking needs to be disabled if using `adb connect`
* Audio on the guest may not work

## Acknowledgements

- mviereck, akvo
