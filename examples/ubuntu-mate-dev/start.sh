#!/bin/bash

# SYNC CONFIG

[ ! -f "$HOME/.profile" ] && cp /etc/skel/.profile $HOME || true
[ ! -f "$HOME/.vimrc" ] && cp /etc/skel/.vimrc $HOME || true
[ ! -d "$HOME/.config" ] && cp -a /etc/skel/.config $HOME || true
[ ! -d "$HOME/.config/fish" ] && cp -a /etc/skel/.config/fish $HOME/.config || true
[ ! -f "$HOME/.config/fish/config.fish" ] && cp /etc/skel/.config/fish/config.fish $HOME/.config/fish || true

# LAUNCH DESKTOP

mate-session

echo "See you soon"
