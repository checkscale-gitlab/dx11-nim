import os
import osproc
import strformat
import common

## Removes the current Docker image and the data files
proc deleteSession*(sessionName: string): bool =
    try:
        if not isValidSessionName(sessionName):
            raise newException(Exception, "The session name must contain letters, numbers, dashes or dots, and start with a letter")

        let sessionPaths = getPathsForSession(sessionName)
        let dockerImageName = fmt"dx11-{sessionName}"

        # Does it exist?
        if not existsDir(sessionPaths.basePath):
            echo "There is no such session: " & sessionName
            return true

        if confirmAction("Do you want to remove the session's Docker image?", false):
            if execShellCmd(fmt"sudo docker image remove {dockerImageName}") != 0:
                return false

        let removeCommand = fmt"sudo rm -Rf {sessionPaths.basePath}"
        echo "\nThis will run\n$ " & removeCommand & "\n"

        if not confirmAction("Do you really want to remove the session's data files?\nThis action cannot be undone", false):
            return false

        echo "Removing " & sessionPaths.basePath

        let exitStatus = execCmd(removeCommand)
        if exitStatus != 0: return false
        return true

    except Exception:
        echo getCurrentExceptionMsg()
        return false
