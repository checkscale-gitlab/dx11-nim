import os
import re

#[

Session data file structure

+ ~/.local/share/dx11/<name>/
| - config.json
| - home-src/
| - home-mount/
| - share/
 `- docker/

]#

# DX11 PATHS

type DX11SessionPaths* = object
    basePath*: string
    configFile*: string
    homeSrcData*: string
    homeEncryptedMount*: string # used only when encryption is enabled
    shareFolder*: string
    dockerFolder*: string

## Computes the paths that correspond to the given session
proc getPathsForSession*(sessionName: string): DX11SessionPaths {.raises: [OSError].} =
    let homeFolder = getEnv("HOME", "")
    if homeFolder == "": raise newException(OSError, "The home folder can't be retrieved")
    result = DX11SessionPaths()
    result.basePath = joinPath(homeFolder, ".local/share/dx11", sessionName)
    result.configFile = joinPath(result.basePath, "config.json")
    result.homeSrcData = joinPath(result.basePath, "home-src")
    result.homeEncryptedMount = joinPath(result.basePath, "home-mount")
    result.shareFolder = joinPath(homeFolder, "Public", "DX11")
    result.dockerFolder = joinPath(result.basePath, "docker")

## Ensures that the corresponding folders exist as expected and checks that the
## config file is not a directory
proc ensureSessionPaths*(sessionPaths: DX11SessionPaths) {.raises: [OSError].} =
    if not existsDir(sessionPaths.basePath):
        let statusCode = execShellCmd("mkdir -p " & sessionPaths.basePath)
        if statusCode != 0: raise newException(OSError, "The session folder could not be created")

    if existsDir(sessionPaths.configFile): raise newException(OSError, "config.json can't be a folder")

    if existsFile(sessionPaths.homeSrcData): raise newException(OSError, "Folder home-src can't be a file")
    elif not existsDir(sessionPaths.homeSrcData):
        let statusCode = execShellCmd("mkdir -p " & sessionPaths.homeSrcData)
        if statusCode != 0: raise newException(OSError, "The home folder could not be created")

    if existsFile(sessionPaths.homeEncryptedMount): raise newException(OSError, "Folder home-mount can't be a file")
    elif not existsDir(sessionPaths.homeEncryptedMount):
        let statusCode = execShellCmd("mkdir -p " & sessionPaths.homeEncryptedMount)
        if statusCode != 0: raise newException(OSError, "The home mount folder could not be created")

    if existsFile(sessionPaths.shareFolder): raise newException(OSError, "The share folder can't be a file")
    elif not existsDir(sessionPaths.shareFolder):
        let statusCode = execShellCmd("mkdir -p " & sessionPaths.shareFolder)
        if statusCode != 0: raise newException(OSError, "The home folder could not be created")

    if existsFile(sessionPaths.dockerFolder): raise newException(OSError, "The docker folder can't be a file")
    elif not existsDir(sessionPaths.dockerFolder):
        let statusCode = execShellCmd("mkdir -p " & sessionPaths.dockerFolder)
        if statusCode != 0: raise newException(OSError, "The home folder could not be created")


let sessionNameRegExp = re(r"^[a-zA-Z][a-zA-Z0-9.\-]*$")

proc isValidSessionName*(sessionName: string):bool =
    return match(sessionName, sessionNameRegExp)

# UTILS

proc confirmAction*(actionMessage: string, defaultsToTrue: bool = false): bool =
    let promptSuffix = if defaultsToTrue: " (Y/n) " else: " (y/N) "
    stdout.write(actionMessage & promptSuffix)

    var response = readLine(stdin)
    case response:
        of "y", "Y", "yes", "Yes", "YES":
            return true
        else: return false
