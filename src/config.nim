import json

import common

type DX11SessionConfig* = object
    useTor*: bool
    encryptHome*: bool

proc readSessionConfig*(sessionName: string): DX11SessionConfig {.raises: [IOError,
        Defect, OSError, ValueError, Exception].} =
    let sessionPaths = getPathsForSession(sessionName)
    let jsonData = parseFile(sessionPaths.configFile)
    return to(jsonData, DX11SessionConfig)

proc writeSessionConfig*(sessionName: string, sessionConfig: DX11SessionConfig) =
    let sessionPaths = getPathsForSession(sessionName)
    writeFile(sessionPaths.configFile, $(%*sessionConfig))
