import os
import osproc
import strformat
import common
import sequtils
import strutils

proc execOnSession*(sessionName: string, command: seq[TaintedString]): bool =
    var sessionPaths: DX11SessionPaths
    try:
        if not isValidSessionName(sessionName):
            raise newException(Exception, "The session name is not valid")

        sessionPaths = getPathsForSession(sessionName)

        if not existsDir(sessionPaths.basePath) or not existsFile(
                sessionPaths.configFile):
            raise newException(OSError, "There is no such session: " & sessionName)

        # Prepare the parameters
        let dockerImageName = fmt"dx11-{sessionName}"

        # Check that the docker image exists
        if execShellCmd("sudo docker image ls | grep " & dockerImageName &
                " > /dev/null") != 0:
            raise newException(OSError, "The docker image does not exist. Try to build it first.")

        # Check that a session is running
        let psResult = execCmdEx(fmt"sudo docker ps | grep {dockerImageName} | cut -d' ' -f1")
        if psResult.exitCode != 0:
            raise newException(Exception, "Could not find a dx11 session to attach to")

        let pid = psResult.output
        if not (pid is string) or pid.len() == 0:
            raise newException(Exception, "Could not find a dx11 session to attach to")

        # Run
        var runCommand = ""
        if command.len() == 0:
            runCommand = fmt"sudo docker exec -it {pid} sh".replace("\n", "")
        else:
            let cmd: string = command.foldr(a & " " & b)
            runCommand = fmt"sudo docker exec -it {pid} {cmd}".replace("\n", "")

        echo "Attaching to: " & dockerImageName

        let exitStatus = execCmd(runCommand)
        return exitStatus == 0

    except Exception:
        echo getCurrentExceptionMsg()
        return false

